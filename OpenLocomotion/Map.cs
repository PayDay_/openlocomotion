﻿using GravityEngine;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenLocomotion
{
    static class Map
    {
		const int MAP_SIZE = 1;
		const int CHUNK_SIZE = 64;

		public const int TILE_WIDTH = 64;
        public const int TILE_WIDTH_HALF = 32;
        public const int TILE_HEIGHT = 32;
        public const int TILE_HEIGHT_HALF = 16;

        public static Chunk[,] Chunks = new Chunk[MAP_SIZE, MAP_SIZE];
		private static TileSet grassSet;

		private static Vector2 NoiseStart;

		public static void Generate()
		{
			grassSet = new TileSet("GRASS1/GRASS1.xml", Vector4.Zero);

			Random random = new Random();
			float startX = (float)random.NextDouble() * 100;
			float startY = (float)random.NextDouble() * 100;
			NoiseStart = new Vector2 (startX, startY);

			for (int y = 0; y < MAP_SIZE; y++)
			{
				for (int x = 0; x < MAP_SIZE; x++)
				{
					Chunks[x, y] = new Chunk(x, y);
				}
			}
		}

		public static Vector3 IsoToScreen(Vector3 point)
		{
			float x = (2 * (point.Y + point.X) * TILE_WIDTH_HALF) / 2;
			float y = (2 * (point.Y - point.X) * TILE_HEIGHT_HALF) / 2;
			float z = y + TILE_HEIGHT_HALF * point.Z;
			return new Vector3 (x, y, z);
		}

		public static Vector3 ScreenToIso(Vector3 point)
		{
			float x = (point.X / TILE_WIDTH_HALF + point.Y / TILE_HEIGHT_HALF) / 2;
            float y = -(point.Y / TILE_HEIGHT_HALF - (point.X / TILE_WIDTH_HALF)) / 2;
            return new Vector3 (y, x, 0);
		}
		public class Chunk
		{
			public Tile[,] Tiles = new Tile[CHUNK_SIZE, CHUNK_SIZE];

			public Chunk(int chunkX, int chunkY)
			{
				for (int y = 0; y < CHUNK_SIZE; y++)
				{
					for (int x = 0; x < CHUNK_SIZE; x++)
					{
						Tiles[x, y] = new Tile(new Vector3(chunkX, chunkY, 0) * CHUNK_SIZE + new Vector3(x, y, 0), grassSet);
					}
				}

				for (int y = 0; y < CHUNK_SIZE; y++)
				{
					for (int x = 0; x < CHUNK_SIZE; x++)
					{
						float noiseX = chunkX * CHUNK_SIZE + NoiseStart.X + x / 4;
						float noiseY = chunkY * CHUNK_SIZE + NoiseStart.Y + y / 4;
                        int height = (int)Math.Round(GravityMath.Perlin.Noise(noiseX, noiseY, 0, 3, 0));
                        //int height = 0;
						Tiles[x, y].SetHeight(0, height);
						if (y > 0)
							Tiles[x, y - 1].SetHeight(1, height);
						if (x > 0)
							Tiles[x - 1, y].SetHeight(3, height);
						if (x > 0 && y > 0)
							Tiles[x - 1, y - 1].SetHeight(2, height);
						/*if (chunkY > 0)
						  Chunks[chunkX, chunkY - 1].Tiles[x, CHUNK_SIZE - 1].SetHeight(1, height);
						if (chunkX > 0)
						  Chunks[chunkX - 1, chunkY].Tiles[CHUNK_SIZE - 1, y].SetHeight(3, height);
						if (chunkX > 0 && chunkY > 0)
						  Chunks[chunkX - 1, chunkY - 1].Tiles[CHUNK_SIZE - 1, CHUNK_SIZE - 1].SetHeight(2, height);*/
						if (y == 0)
							if (chunkY > 0)
							{
								Chunks[chunkX, chunkY - 1].Tiles[x, CHUNK_SIZE - 1].SetHeight(2, height);
								if (x < CHUNK_SIZE - 1)
									Chunks[chunkX, chunkY - 1].Tiles[x + 1, CHUNK_SIZE - 1].SetHeight(1, height);
							}
						if (x == 0)
							if (chunkX > 0)
							{
								Chunks[chunkX - 1, chunkY].Tiles[CHUNK_SIZE - 1, y].SetHeight(2, height);
								if (y < CHUNK_SIZE - 1)
									Chunks[chunkX - 1, chunkY].Tiles[CHUNK_SIZE - 1, y + 1].SetHeight(3, height);
							}
						if (x == 0 && y == 0)
							if (chunkX > 0 && chunkY > 0)
								Chunks[chunkX - 1, chunkY - 1].Tiles[CHUNK_SIZE - 1, CHUNK_SIZE - 1].SetHeight(2, height);
					}
				}
			}
		}
    }
}
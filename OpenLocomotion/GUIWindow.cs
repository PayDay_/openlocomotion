﻿using System;
using System.Drawing;
using OpenTK;
using GravityEngine;
using System.Collections.ObjectModel;

namespace OpenLocomotion
{
	public class GUIWindow : Entity
    {
        public static ObservableCollection<GUIWindow> GUIWindows = new ObservableCollection<GUIWindow>();

		static Texture textureWindow = new Texture("GUI/window.png", true, false);
        static Texture textureButtonClose = new Texture("GUI/buttonClose.png", false, true);

		static WindowBorders windowBorders = new WindowBorders(30, 4, 4, 4);
		static CaptionMargin captionMargin = new CaptionMargin(4, 4, 6, 4);

        SlicedSprite sprite;

		Entity captionEntity;
		Label captionLabel;

        Entity barButtonEntity;
        Button barButton;

        Entity buttonCloseEntity;
        SpriteRenderer buttonCloseSprite;
        Button buttonClose;

        bool draggingPosition;

		private Size size;
		public Size Size
		{
			get
			{
				return size;
			}
			set
			{
				this.size = value;
				this.sprite.Size = value;

				captionEntity.Transform.Position = new Vector3(captionMargin.Left, value.Height - windowBorders.Top + captionMargin.Bottom, captionEntity.Transform.Position.Z);

				barButtonEntity.Transform.Position = new Vector3(0, value.Height - windowBorders.Top, barButtonEntity.Transform.Position.Z);
				barButton.Size = new Size(value.Width, windowBorders.Top);

            	int buttonSize = windowBorders.Top - windowBorders.Right * 2;
				buttonCloseEntity.Transform.Position = new Vector3(value.Width - buttonSize - windowBorders.Right, windowBorders.Right, buttonCloseEntity.Transform.Position.Z);

				this.Transform.Position = this.Transform.Position; //TODO: Very Ugly

				this.draggingPosition = false;
			}
		}

		public GUIWindow(string caption, Size size) : base(Space.Screen, null)
		{
            Transform.Space = Space.Screen;

			this.size = size;

            sprite = new SlicedSprite(this, size, textureWindow, windowBorders, 2, 22, 2, 2);

			captionEntity = new Entity(Space.Screen, Transform);
			captionLabel = new Label(captionEntity, caption, Window.SourceSansProBold, windowBorders.Top - captionMargin.Top - captionMargin.Bottom, StringAlignment.Near, Vector4.One);
			captionEntity.Transform.Position = new Vector3(captionMargin.Left, size.Height - windowBorders.Top + captionMargin.Bottom, -1);

            barButtonEntity = new Entity(new Vector3(0, size.Height - windowBorders.Top, -1), Transform, Space.Screen);
            barButton = new Button(barButtonEntity, new Size(size.Width, windowBorders.Top));
            barButton.MouseDown += BarButton_MouseDown;
            barButton.MouseUp += BarButton_MouseUp;

            int buttonSize = windowBorders.Top - windowBorders.Right * 2;
            buttonCloseEntity = new Entity(new Vector3(size.Width - buttonSize - windowBorders.Right, windowBorders.Right, 0), barButtonEntity.Transform, Space.Screen);
            buttonCloseSprite = new SpriteRenderer(buttonCloseEntity, new Size(buttonSize, buttonSize), textureButtonClose);
            buttonClose = new Button(buttonCloseEntity, new Size(buttonSize, buttonSize));
            buttonClose.MouseDown += ButtonClose_MouseDown;

			GravityWindow.Window.MouseMove += Window_MouseMove;

            GUIWindows.CollectionChanged += GUIWindows_CollectionChanged;
            GUIWindows.Insert(0, this); //On top of all other windows
        }

        private void GUIWindows_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Transform.Position = new Vector3(Transform.Position.X, Transform.Position.Y, GUIWindows.IndexOf(this) * 10);
        }

        public void Focus()
        {
            GUIWindows.Move(GUIWindows.IndexOf(this), 0);
        }

        public void Center()
		{
			this.Transform.Position = new Vector3(
				GravityWindow.Window.Size.Width / 2 - this.Size.Width / 2,
				GravityWindow.Window.Size.Height / 2 - this.Size.Height / 2,
				0
			);
		}

        private void ButtonClose_MouseDown(object sender, EventArgs e)
        {
        }

        private void Window_MouseMove(object sender, OpenTK.Input.MouseMoveEventArgs e)
        {
            if (draggingPosition)
            {
                Transform.Position += new Vector3(e.XDelta, -e.YDelta, 0);
            }
        }

        private void BarButton_MouseDown(object sender, EventArgs e)
        {
            draggingPosition = true;
            GameCursor.CursorState = GameCursor.State.MOVE;
            Focus();
        }

        private void BarButton_MouseUp(object sender, EventArgs e)
        {
            draggingPosition = false;
            GameCursor.CursorState = GameCursor.State.NORMAL;
        }
    }

	public struct CaptionMargin
	{
		public int Top;
		public int Right;
		public int Bottom;
		public int Left;

		public CaptionMargin(int Top, int Right, int Bottom, int Left)
		{
			this.Left = Left;
			this.Top = Top;
			this.Right = Right;
			this.Bottom = Bottom;
		}
	}
}

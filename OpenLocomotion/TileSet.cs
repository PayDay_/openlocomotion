﻿using GravityEngine;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace OpenLocomotion
{
    public class TileSet
    {
        public Point[] SpriteOffsets = new Point[0];
        public Rectangle[] SpriteRectangles = new Rectangle[0];
        public Texture TextureAtlas;

		public TileSet(string xmlPath, Vector4 transparencyReplacement)
        {
            Parse(xmlPath);

			TextureAtlas = new Texture(Path.GetFileNameWithoutExtension(xmlPath) + "/" + Path.GetFileNameWithoutExtension(xmlPath) + ".png", true, true, TextureWrapMode.Clamp, transparencyReplacement);
        }

        private void Parse(string xmlPath)
        {
            XDocument doc = XDocument.Load(xmlPath);
            XElement[] sprites = doc.Root.Element("chunk").Elements("sprite").ToArray();

            SpriteRectangles = new Rectangle[sprites.Length];
            SpriteOffsets = new Point[sprites.Length];
            for(int i = 0; i < sprites.Length; i++)
            {
                int xOffset = int.Parse(sprites[i].Attribute("xofs").Value);
                int yOffset = int.Parse(sprites[i].Attribute("yofs").Value);

                //Ugly fix
                if (i == 361) yOffset += 1;
				else if (i == 362) yOffset += 15;
                else if (i == 363) yOffset += 17;
                else if (i == 364) yOffset += 1;
                else if (i == 365) yOffset += 1;
                else if (i == 366) yOffset += 17;
                else if (i == 367) yOffset += 17;
                else if (i == 368) yOffset += 16;
                else if (i == 369) yOffset += 16;
                else if (i == 370) yOffset += 32;
                else if (i == 371) yOffset += 32;
                else if (i == 372) yOffset += 16;
                else if (i == 373) yOffset += 16;
                else if (i == 374) yOffset += 32;

                else if (i == 20)
                    yOffset += 1;
                else if (i == 22)
                    yOffset += 2;
                else if (i == 23)
                    yOffset += 2;

                else if (i == 24)
                {
                    yOffset += -5;
                }
                else if (i == 25)
                {
                    xOffset += 30;
                    yOffset += 10;
                }
                else if (i == 26)
                {
                    xOffset += 33;
                    yOffset += -8;
                }
                else if (i == 27)
                {
                    xOffset += 63;
                    yOffset += -6;
                }

                else if (i == 40)
                {
                    yOffset += -6;
                }
                else if (i == 42)
                {
                    xOffset += 32;
                    yOffset += -10;
                }
                else if (i == 43)
                {
                    xOffset += 64;
                    yOffset += -5;
                }

                else if (i == 56)
                {
                    yOffset += -6;
                }
                else if (i == 58)
                {
                    xOffset += 32;
                    yOffset += -11;
                }
                else if (i == 59)
                {
                    xOffset += 66;
                    yOffset += -6;
                }

                SpriteOffsets[i] = new Point(xOffset, yOffset);

                XElement pngImage = sprites[i].Element("pngimage");
                if (pngImage != null)
                {
                    int x = int.Parse(pngImage.Attribute("xofs").Value);
                    int y = int.Parse(pngImage.Attribute("yofs").Value);
                    int width = int.Parse(pngImage.Attribute("width").Value);
                    int height = int.Parse(pngImage.Attribute("height").Value);
                    SpriteRectangles[i] = new Rectangle(x, y, width, height);
                }
                else
                    SpriteRectangles[i] = new Rectangle();
            }
        }
    }
}

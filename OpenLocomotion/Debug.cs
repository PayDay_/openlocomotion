﻿using System;
using GravityEngine;
using OpenTK;

namespace OpenLocomotion
{
	public static class Debug
	{
		static GUIWindow window;

		static Entity labelTransformer;
		static Entity inputTransformer;
		static Label label;
		static TextField input;

		public delegate void ExecutionEventHandler(ExecutionEventArgs e);
		public static event ExecutionEventHandler Execution;

		public static void Init()
		{
			window = new GUIWindow("Console", new System.Drawing.Size(600, 400));

			labelTransformer = new Entity(Space.Screen, window.Transform);
			labelTransformer.Transform.Position = new Vector3(10, 400 - 30 - 24, -1);
			label = new Label(labelTransformer, "", Window.SourceSansPro, 16, System.Drawing.StringAlignment.Near, new Vector4(0, 0, 0, 1));

			label.Text = "OpenLoco DebugConsole v0.1...";

			inputTransformer = new Entity(Space.Screen, window.Transform);
			inputTransformer.Transform.Position = new Vector3(10, 10, -1);
			input = new TextField(inputTransformer, new System.Drawing.Size(window.Size.Width - 20, 40), Window.SourceSansPro, 16, System.Drawing.StringAlignment.Near, new Vector4(0, 0, 0, 1));

			input.Enter += Execute;
		}

		public static void Log(string text)
		{
			label.Text += "\n" + text;
		}

		public static void Execute(object sender, EventArgs e)
		{
			ExecutionEventArgs args = new ExecutionEventArgs();
			args.Command = input.Text;
			Execution(args);
		}
	}
	public class ExecutionEventArgs : EventArgs
	{
		public string Command { get; set; }
	}
}

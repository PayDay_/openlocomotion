﻿using System;
using GravityEngine;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;

namespace OpenLocomotion
{
    public static class GameCursor
    {
        static Dictionary<State, Bitmap> cursorBitmaps = new Dictionary<State, Bitmap>();

        public static void Init()
        {
            float scaling = Screen.ScalingFactor - 0.73f;
            scaling = 0.35f;

            Image image = Image.FromFile("Cursors/arrow.png");
            Bitmap bitmap = new Bitmap(image, new Size((int)(image.Width * scaling), (int)(image.Height * scaling)));
            cursorBitmaps.Add(State.NORMAL, bitmap);
            image = Image.FromFile("Cursors/move.png");
            bitmap = new Bitmap(image, new Size((int)(image.Width * scaling), (int)(image.Height * scaling)));
            cursorBitmaps.Add(State.MOVE, bitmap);

            CursorState = State.NORMAL;
        }

        private static State cursorState = State.NORMAL;
        public static State CursorState
        {
            get { return cursorState; }
            set
            {
                cursorState = value;
                Bitmap bitmap = cursorBitmaps[value];
                BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                int hotX = 0, hotY = 0;
                if(value == State.MOVE)
                {
                    hotX = bitmap.Width / 2;
                    hotY = bitmap.Height / 2;
                }

                Window.Window.Cursor = new OpenTK.MouseCursor(hotX, hotY, bitmap.Width, bitmap.Height, data.Scan0);
                bitmap.UnlockBits(data);
            }
        }

        public enum State
        {
            NORMAL,
            MOVE,
        }
    }
}

﻿using System.Collections.Generic;

namespace OpenLocomotion
{
	public class MultiLangString
	{
		private Dictionary<int, string> strings = new Dictionary<int, string>();

		public MultiLangString(Dictionary<int, string> strings)
		{
			this.strings = strings;
		}

		public override string ToString()
		{
			return strings[0];
		}
	}
}
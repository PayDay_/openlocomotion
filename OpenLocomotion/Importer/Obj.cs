﻿using System;
namespace OpenLocomotion
{
	public class Obj
	{
		public ObjType Type
		{
			private set;
			get;
		}

		public Obj(ObjType Type)
		{
			this.Type = Type;
		}
	}

	public enum ObjType
	{
		INTERFACES,                 // 0x00
		SOUND_EFFECTS,
		CURRENCIES,
		EXHAUST_EFFECTS,
		CLIFF_FACES,
		WATER,
		GROUND,
		TOWN_NAMES,
		CARGOES,
		FENCES,
		SIGNALS,
		CROSSINGS,
		STREET_LIGHTS,
		TUNNELS,
		BRIDGES,
		TRAIN_STATIONS,
		TRACK_MODIFICATIONS,        // 0x10
		TRACKS,
		ROAD_STATIONS,
		ROAD_MODIFICATIONS,
		ROADS,
		AIRPORTS,
		DOCKS,
		VEHICLES,
		TREES,
		SNOW,
		CLIMATES,
		SHAPES,
		BUILDINGS,
		SCAFFOLDING,
		INDUSTRIES,
		REGIONS,
		COMPANIES,                  // 0x20
		TEXTS
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OpenLocomotion
{
	public static class ObjImport
	{
		/// <summary>
		/// Load a .DAT from a specific path.
		/// </summary>
		/// <returns>The file as byte array.</returns>
		/// <param name="path">The Path.</param>
		public static ObjInputStream Load(string path)
		{
			if (!File.Exists("ObjData/" + path))
				Debug.Log("ERROR: File doesn't exist!");
			
			return new ObjInputStream(File.Open("ObjData/" + path, FileMode.Open));
		}

		public static Obj Import(ObjInputStream data)
		{
			ObjType type = (ObjType)(data.ReadByte() & 0x7f);

			data.SkipBytes(2);
			if (data.ReadByte() != 0x11) Debug.Log("WARNING: Not an original file!");

			Obj gameObject = new Obj(type);
			Debug.Log("Imported an .DAT from type " + type.ToString() + ".");
			if (type == ObjType.VEHICLES)
			{
				ImportVehicle("", data);
			}
			return gameObject;
		}

		public static Vehicle ImportVehicle(String name, ObjInputStream fs)
		{
			VehicleVars vars = LoadVehicleVars(fs);
			/*MultiLangString description = inputStream.readMultiLangString();
			UseObject trackType = null;
        	if (vars.getVehicleClass() < 2 && !vars.getVehicleFlags().contains(VehicleVars.VehicleFlag.ANYTRACK)) {
				trackType = inputStream.readUseObject(EnumSet.of(ObjectClass.TRACKS, ObjType.ROADS));
			}
			//optional reference to track/road modification?
			List<UseObject> trackModifications = inputStream.readUseObjectList(vars.getNumMods(), ObjType.TRACK_MODIFICATIONS, ObjType.ROAD_MODIFICATIONS);

			List<CargoCapacity> cargoCapacities = new ArrayList<>();
	        for (int i = 0; i<2; i++) {
	            cargoCapacities.add(loadCargoCapacity(inputStream));
	        }

			UseObject visualFx = null;
	        if (vars.getVisFxType() != 0) {
	            visualFx = inputStream.readUseObject(EnumSet.of(ObjType.EXHAUST_EFFECTS));
	        }

			UseObject wakeFx = null;
	        if (vars.getWakeFxType() != 0) {
	            wakeFx = inputStream.readUseObject(EnumSet.of(ObjType.EXHAUST_EFFECTS));
	        }

	        UseObject rackRail = null;
	        if (vars.getVehicleClass() < 2 && vars.getVehicleFlags().contains(VehicleVars.VehicleFlag.RACKRAIL)) {
	            rackRail = inputStream.readUseObject(EnumSet.of(ObjType.TRACK_MODIFICATIONS));
	        }

        	List<UseObject> compatibleVehicles = inputStream.readUseObjectList(vars.getNumCompat(), ObjType.VEHICLES);

			UseObject startSnd = null;
	        if (vars.getStartsndtype() != 0) {
	            startSnd = inputStream.readUseObject(EnumSet.of(ObjType.SOUND_EFFECTS));
	        }

        	int soundCount = vars.getNumSnd() & 0x7f;
			List<UseObject> sounds = inputStream.readUseObjectList(soundCount, ObjType.SOUND_EFFECTS);

			Sprites sprites = loadSprites(inputStream);

        	return new Vehicle(name, description, vars, trackType, trackModifications, cargoCapacities, visualFx, wakeFx, rackRail, startSnd, compatibleVehicles, sounds, sprites);*/
			return new Vehicle();
	    }

		private static VehicleVars LoadVehicleVars(ObjInputStream stream)
		{
			stream.SkipBytes(2);

			byte vehicleClass = stream.ReadByte();
			Debug.Log("VehicleClass: " + vehicleClass);
			byte vehicleType = stream.ReadByte();
			byte numVehicleUnits = stream.ReadByte();
	        stream.SkipBytes(1);
			byte numMods = stream.ReadByte();
			byte costInd = stream.ReadByte();
			short costFact = stream.ReadSShort();
			byte reliability = stream.ReadByte();
			byte runCostInd = stream.ReadByte();
			short runCostFact = stream.ReadSShort();
			byte colourType = stream.ReadByte();
			byte numCompat = stream.ReadByte();
	        stream.SkipBytes(20);
			List<VehicleUnit> vehicleUnits = new List<VehicleUnit>();
	        for (int i = 0; i<4; i++) {
				vehicleUnits.Add(LoadVehicleUnit(stream));
	        }
			/*List<VehicleUnitSpriteDetails> vehicleUnitSpriteDetails = new ArrayList<>();
	        for (int i = 0; i<4; i++) {
	            vehicleUnitSpriteDetails.add(loadVehicleUnitSpriteDetails(in));
	        }
			List<VehicleBogeySpriteDetails> vehicleBogeySpriteDetails = new ArrayList<>();
	        for (int i = 0; i<2; i++) {
	            vehicleBogeySpriteDetails.add(loadVehicleBogeySpriteDetails(in));
	        }
	        int power = in.readUShort();
			int speed = in.readUShort();
			int rackSpeed = in.readUShort();
			int weight = in.readUShort();
			EnumSet<VehicleVars.VehicleFlag> vehicleFlags = in.readBitField(2, VehicleVars.VehicleFlag.class);
	        in.skipBytes(44);
			byte visFxHeight = in.readByte();
			byte visFxType = in.readByte();
	        in.skipBytes(2);
			byte wakeFxType = in.readByte();
	        in.skipBytes(1);
			int designed = in.readUShort();
			int obsolete = in.readUShort();
	        in.skipBytes(1);
			byte startsndtype = in.readByte();
	        in.skipBytes(64);
			byte numSnd = in.readByte();
	        in.skipBytes(3);

	        return new VehicleVars(vehicleClass, vehicleType, numVehicleUnits, numMods, costInd, costFact, reliability, runCostInd,
					runCostFact, colourType, numCompat, vehicleUnits, vehicleUnitSpriteDetails, power, speed, rackSpeed, weight, vehicleFlags,
					visFxHeight, visFxType, wakeFxType, designed, obsolete, startsndtype, numSnd);*/
			return new VehicleVars(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, null, 0, 0, 0, 0, 0, 0, 0);
	    }

		private static VehicleUnit LoadVehicleUnit(ObjInputStream stream)
		{
			byte length = stream.ReadByte();
			byte rearBogiePosition = stream.ReadByte();
			byte frontBogeyIndex = stream.ReadByte();
			byte rearBogeyIndex = stream.ReadByte();
			byte spriteDetailsIndex = stream.ReadByte();
			byte effectPosition = stream.ReadByte();
	        return new VehicleUnit(length, rearBogiePosition, frontBogeyIndex, rearBogeyIndex, spriteDetailsIndex, effectPosition);
		}

		private static VehicleUnitSpriteDetails LoadVehicleUnitSpriteDetails(ObjInputStream stream)
		{
	        byte levelSpriteCount = stream.ReadByte();
	        byte upDownSpriteCount = stream.ReadByte();
	        byte frames = stream.ReadByte();
	        byte vehType = stream.ReadByte();
	        byte numUnits = stream.ReadByte();
	        byte tiltCount = stream.ReadByte();
	        byte bogeyPos = stream.ReadByte();
			List<VehicleUnitSpriteDetails.VehicleSpriteFlag> flags = null; //stream.ReadBitField(1, VehicleUnitSpriteDetails.VehicleSpriteFlag.class); //TODO: i can't sharpen it
	        stream.SkipBytes(6);
			byte spriteNum = stream.ReadByte();
	        stream.SkipBytes(15);
	        return new VehicleUnitSpriteDetails(levelSpriteCount, upDownSpriteCount, frames, vehType, numUnits, tiltCount, bogeyPos, flags, spriteNum);
		}

		/*private static VehicleVars loadVehicleVars(DatFileInputStream in) throws IOException
		{
	        in.skipBytes(2);
	        byte vehicleClass = in.readByte();
	        byte vehicleType = in.readByte();
	        byte numVehicleUnits = in.readByte();
	        in.skipBytes(1);
	        byte numMods = in.readByte();
	        byte costInd = in.readByte();
	        short costFact = in.readSShort();
	        byte reliability = in.readByte();
	        byte runCostInd = in.readByte();
	        short runCostFact = in.readSShort();
	        byte colourType = in.readByte();
	        byte numCompat = in.readByte();
	        in.skipBytes(20);
			List<VehicleUnit> vehicleUnits = new ArrayList<>();
	        for (int i = 0; i<4; i++) {
	            vehicleUnits.add(loadVehicleUnit(in));
	        }
	        List<VehicleUnitSpriteDetails> vehicleUnitSpriteDetails = new ArrayList<>();
	        for (int i = 0; i<4; i++) {
	            vehicleUnitSpriteDetails.add(loadVehicleUnitSpriteDetails(in));
	        }
	        List<VehicleBogeySpriteDetails> vehicleBogeySpriteDetails = new ArrayList<>();
	        for (int i = 0; i<2; i++) {
	            vehicleBogeySpriteDetails.add(loadVehicleBogeySpriteDetails(in));
	        }
	        int power = in.readUShort();
			int speed = in.readUShort();
			int rackSpeed = in.readUShort();
			int weight = in.readUShort();
			EnumSet<VehicleVars.VehicleFlag> vehicleFlags = in.readBitField(2, VehicleVars.VehicleFlag.class);
	        in.skipBytes(44);
			byte visFxHeight = in.readByte();
			byte visFxType = in.readByte();
	        in.skipBytes(2);
			byte wakeFxType = in.readByte();	
	        in.skipBytes(1);
			int designed = in.readUShort();
			int obsolete = in.readUShort();
	        in.skipBytes(1);
			byte startsndtype = in.readByte();
	        in.skipBytes(64);
			byte numSnd = in.readByte();
	        in.skipBytes(3);

	        return new VehicleVars(vehicleClass, vehicleType, numVehicleUnits, numMods, costInd, costFact, reliability, runCostInd,
					runCostFact, colourType, numCompat, vehicleUnits, vehicleUnitSpriteDetails, power, speed, rackSpeed, weight, vehicleFlags,
					visFxHeight, visFxType, wakeFxType, designed, obsolete, startsndtype, numSnd);
		    }

	    private static VehicleBogeySpriteDetails loadVehicleBogeySpriteDetails(DatFileInputStream in) throws IOException
		{
	        byte animFrames = in.readByte();
			EnumSet<VehicleUnitSpriteDetails.VehicleSpriteFlag> flags = in.readBitField(1, VehicleUnitSpriteDetails.VehicleSpriteFlag.class);
	        in.skipBytes(16);
	        return new VehicleBogeySpriteDetails(animFrames, flags);
	    }*/
	}
}

﻿using System.Collections.Generic;

namespace OpenLocomotion
{
	public class VehicleUnitSpriteDetails
	{

		public byte levelSpriteCount { get; private set; }
		public byte upDownSpriteCount { get; private set; }
		public byte frames { get; private set; }
		public byte cargoLoadingStates { get; private set; } // number of frames between full and empty
		public byte cargoLoadingFrames { get; private set; } // total number of cargo frames (empty to full for each cargo)
		public byte tiltCount { get; private set; }
		public byte bogeyPos { get; private set; }
		public List<VehicleSpriteFlag> flags { get; private set; }
		public byte spriteNum { get; private set; }

		public VehicleUnitSpriteDetails(byte levelSpriteCount, byte upDownSpriteCount, byte frames, byte cargoLoadingStates, byte cargoLoadingFrames,
										byte tiltCount, byte bogeyPos, List<VehicleSpriteFlag> flags, byte spriteNum)
		{
			this.levelSpriteCount = levelSpriteCount;
			this.upDownSpriteCount = upDownSpriteCount;
			this.frames = frames;
			this.cargoLoadingStates = cargoLoadingStates;
			this.cargoLoadingFrames = cargoLoadingFrames;
			this.tiltCount = tiltCount;
			this.bogeyPos = bogeyPos;
			this.flags = flags;
			this.spriteNum = spriteNum;
		}

		public enum VehicleSpriteFlag
		{
			HAS_SPRITES,
			IS_SYMMETRICAL,
			unknown2,
			unknown3,
			unknown4,
			REVERSED
		}
	}
}
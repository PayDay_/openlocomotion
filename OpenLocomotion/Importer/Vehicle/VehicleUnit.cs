﻿namespace OpenLocomotion
{
	public class VehicleUnit
	{
		public byte length { private set; get; }
		public byte rearBogeyPosition { private set; get; }
		public byte frontBogeyIndex { private set; get; }
		public byte rearBogeyIndex { private set; get; }
		public bool isSpriteDetailsReversed { private set; get; }
    	public bool isSpacingOnly { private set; get; }
    	public int spriteDetailsIndex { private set; get; }
		public byte effectPosition { private set; get; }

		public VehicleUnit(byte length, byte rearBogeyPosition, byte frontBogeyIndex, byte rearBogeyIndex, int spriteDetailsIndex, byte effectPosition)
		{
			this.length = length;
			this.rearBogeyPosition = rearBogeyPosition;
			this.frontBogeyIndex = frontBogeyIndex;
			this.rearBogeyIndex = rearBogeyIndex;
			if (spriteDetailsIndex < 0)
			{
				if (spriteDetailsIndex > -5)
				{
					this.spriteDetailsIndex = -(spriteDetailsIndex + 1);
					isSpriteDetailsReversed = false;
					isSpacingOnly = true;
				}
				else
				{
					this.spriteDetailsIndex = spriteDetailsIndex + 128;
					isSpriteDetailsReversed = true;
					isSpacingOnly = false;
				}
			}
			else
			{
				this.spriteDetailsIndex = spriteDetailsIndex;
				isSpriteDetailsReversed = false;
				isSpacingOnly = false;
			}
			this.effectPosition = effectPosition;
		}
	}
}
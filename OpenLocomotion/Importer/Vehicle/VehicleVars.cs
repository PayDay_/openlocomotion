﻿using System.Collections.Generic;

namespace OpenLocomotion
{
	public class VehicleVars
	{
		public enum VehicleFlag
		{
			IS_LOCO, // is loco
			INVERT_SECOND_LOCO, // invert second loco?
			unknown3, // loco only at head of train?
			unknown4,
			unknown5,
			REQUIRES_DRIVING_CAR, // power car in middle
			RACKRAIL,
			unknown7, unknown8,
			ANYTRACK,
			unknown9,
			CANCOUPLE,
			DUALHEAD,
			IS_STEAM, //makes chuffing noises
			REFITTABLE,
			NOANNOUNCE
		}

		public byte vehicleClass { get; private set; }
		public byte vehicleType { get; private set; }
		public byte numVehicleUnits { get; private set; }
		public byte numMods { get; private set; }
		public byte costInd { get; private set; }
		public short costFact { get; private set; }
		public byte reliability { get; private set; }
		public byte runCostInd { get; private set; }
		public short runCostFact { get; private set; }
		public byte colourType { get; private set; }
		public byte numCompat { get; private set; }
		public List<VehicleUnit> vehicleUnits { get; private set; }
		public List<VehicleUnitSpriteDetails> vehicleUnitSpriteDetails { get; private set; }
		public int power { get; private set; }
		public int speed { get; private set; }
		public int rackSpeed { get; private set; }
		public int weight { get; private set; }
		public List<VehicleFlag> vehicleFlags { get; private set; }
		public byte visFxHeight { get; private set; }
		public byte visFxType { get; private set; }
		public byte wakeFxType { get; private set; }
		public int designed { get; private set; }
		public int obsolete { get; private set; }
		public byte startsndtype { get; private set; }
		public byte numSnd { get; private set; }

		public VehicleVars(byte vehicleClass, byte vehicleType, byte numVehicleUnits, byte numMods, byte costInd, short costFact,
						   byte reliability, byte runCostInd, short runCostFact, byte colourType, byte numCompat,
						   List<VehicleUnit> vehicleUnits, List<VehicleUnitSpriteDetails> vehicleUnitSpriteDetails, int power, int speed, int rackSpeed, int weight,
		                   List<VehicleFlag> vehicleFlags, byte visFxHeight, byte visFxType, byte wakeFxType,
						   int designed, int obsolete, byte startsndtype, byte numSnd)
		{
			this.vehicleClass = vehicleClass;
			this.vehicleType = vehicleType;
			this.numVehicleUnits = numVehicleUnits;
			this.numMods = numMods;
			this.costInd = costInd;
			this.costFact = costFact;
			this.reliability = reliability;
			this.runCostInd = runCostInd;
			this.runCostFact = runCostFact;
			this.colourType = colourType;
			this.numCompat = numCompat;
			this.vehicleUnits = vehicleUnits;
			this.vehicleUnitSpriteDetails = vehicleUnitSpriteDetails;
			this.power = power;
			this.speed = speed;
			this.rackSpeed = rackSpeed;
			this.weight = weight;
			this.vehicleFlags = vehicleFlags;
			this.visFxHeight = visFxHeight;
			this.visFxType = visFxType;
			this.wakeFxType = wakeFxType;
			this.designed = designed;
			this.obsolete = obsolete;
			this.startsndtype = startsndtype;
			this.numSnd = numSnd;
		}
	}
}
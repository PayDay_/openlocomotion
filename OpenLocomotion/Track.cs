﻿using System;
using OpenTK;
using GravityEngine;
using System.Collections.Generic;

namespace OpenLocomotion
{
	public class Track : MapEntity
	{
		static TileSet trackst;
		private SpriteRenderer[] sprites;

        private static Dictionary<TrackType, List<int>> trackSprites;

		public Track(TrackType type, Transform parent = null) : base(parent)
		{
            sprites = new SpriteRenderer[trackSprites[type].Count];

            for(int i = 0; i < sprites.Length; i++)
            {
                sprites[i] = new SpriteRenderer(this, trackst.SpriteRectangles[trackSprites[type][i]].Size, trackst.TextureAtlas, trackst.SpriteRectangles[trackSprites[type][i]]);
                sprites[i].Pivot = new Vector2(trackst.SpriteOffsets[trackSprites[type][i]].X, trackst.SpriteOffsets[trackSprites[type][i]].Y);
            }
        }

		public static void Load()
		{
			trackst = new TileSet("trackst/trackst.xml", new Vector4(0, 0, 1, 1));

            trackSprites = new Dictionary<TrackType, List<int>>();

            List<int> spriteList;
            #region Data
            spriteList = new List<int>();
            spriteList.Add(18);
            spriteList.Add(20);
            spriteList.Add(22);
            trackSprites.Add(TrackType.STRAIGHT1, spriteList);

            spriteList = new List<int>();
            spriteList.Add(19);
            spriteList.Add(21);
            spriteList.Add(23);
            trackSprites.Add(TrackType.STRAIGHT2, spriteList);

            spriteList = new List<int>();
            spriteList.Add(24);
            spriteList.Add(25);
            spriteList.Add(26);
            spriteList.Add(27);
            spriteList.Add(40);
            //spriteList.Add(41);
            spriteList.Add(42);
            spriteList.Add(43);
            spriteList.Add(56);
            spriteList.Add(58);
            spriteList.Add(59);
            trackSprites.Add(TrackType.CURVE_SMALL1, spriteList);
            #endregion
        }

        public enum TrackType
        {
            STRAIGHT1,
            STRAIGHT2,
            CURVE_SMALL1,
        }
	}
}

﻿using System;
using GravityEngine;

namespace OpenLocomotion
{
	class Program
	{
		public static Window Window;

		public static void Main (string[] args)
		{
			Window = new Window ();
			Window.Run ();
		}
	}
}

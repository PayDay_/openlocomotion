﻿using System;
using GravityEngine;
using OpenTK;
using System.Drawing;

namespace OpenLocomotion
{
	public class Tile : Entity
    {
        private Vector3 position;
        public Vector3 Position
        {
            get { return position; }
            set
            {
                position = value;
                UpdatePosition();
            }
        }

        public int[] Heights { get; private set; } = new int[4];
        public int BaseHeight;

        private TileSet tileSet;

        public SpriteRenderer Sprite;
        private int spriteID;

		public Tile (Vector3 position, TileSet tileSet) : base(position, null)
		{
            Position = position;

            Sprite = new SpriteRenderer(this, new Size(64, 32), tileSet.TextureAtlas, tileSet.SpriteRectangles[360]);
            SetTileSet(tileSet);
		}

        private void UpdatePosition()
        {
            //float posX = Position.X * 32 + Position.Y * 32;
            //float posY = -Position.X * 16 + Position.Y * 16;
			Vector3 pos = Map.IsoToScreen (Position);

			pos.Y += BaseHeight * 16;

            if (Sprite != null)
            {
				pos.X += tileSet.SpriteOffsets[spriteID].X;
				pos.Y += tileSet.SpriteOffsets[spriteID].Y;
            }

			Transform.Position = pos;
            //Transform.Position = new Vector2(posX, posY);
        }

        public void SetHeights(int c0, int c1, int c2, int c3)
        {
            Heights = new int[] { c0, c1, c2, c3 };
            UpdateTile();
        }

        public void SetHeight(int corner, int height)
        {
            Heights[corner] = height;
            UpdateTile();
        }

        private void UpdateTile()
        {
            int highest = int.MinValue;
            for (int i = 0; i < 4; i++)
                highest = Math.Max(highest, Heights[i]);

            for (int i = 0; i < 4; i++)
            {
                if (Heights[i] < highest - 1)
                    Heights[i] = highest - 1;
            }

            highest = int.MinValue;
            for (int i = 0; i < 4; i++)
                highest = Math.Max(highest, Heights[i]);

            int lowest = int.MaxValue;
            for (int i = 0; i < 4; i++)
                lowest = Math.Min(lowest, Heights[i]);

            //BaseHeight = highest;
            BaseHeight = lowest;

            int[] relativeHeights = new int[4];
            for (int i = 0; i < 4; i++)
                relativeHeights[i] = this.Heights[i] - lowest;

            if (relativeHeights[0] == 0 && relativeHeights[1] == 0 && relativeHeights[2] == 0 && relativeHeights[3] == 0)
                spriteID = 360;
            else if (relativeHeights[0] == 1 && relativeHeights[1] == 0 && relativeHeights[2] == 0 && relativeHeights[3] == 0)
                spriteID = 361;
            else if (relativeHeights[0] == 0 && relativeHeights[1] == 0 && relativeHeights[2] == 0 && relativeHeights[3] == 1)
                spriteID = 362;
            else if (relativeHeights[0] == 1 && relativeHeights[1] == 0 && relativeHeights[2] == 0 && relativeHeights[3] == 1)
                spriteID = 363;
            else if (relativeHeights[0] == 0 && relativeHeights[1] == 0 && relativeHeights[2] == 1 && relativeHeights[3] == 0)
                spriteID = 364;
            else if (relativeHeights[0] == 1 && relativeHeights[1] == 0 && relativeHeights[2] == 1 && relativeHeights[3] == 0)
                spriteID = 365;
            else if (relativeHeights[0] == 0 && relativeHeights[1] == 0 && relativeHeights[2] == 1 && relativeHeights[3] == 1)
                spriteID = 366;
            else if (relativeHeights[0] == 1 && relativeHeights[1] == 0 && relativeHeights[2] == 1 && relativeHeights[3] == 1)
                spriteID = 367;
            else if (relativeHeights[0] == 0 && relativeHeights[1] == 1 && relativeHeights[2] == 0 && relativeHeights[3] == 0)
                spriteID = 368;
            else if (relativeHeights[0] == 1 && relativeHeights[1] == 1 && relativeHeights[2] == 0 && relativeHeights[3] == 0)
                spriteID = 369;
            else if (relativeHeights[0] == 0 && relativeHeights[1] == 1 && relativeHeights[2] == 0 && relativeHeights[3] == 1)
                spriteID = 370;
            else if (relativeHeights[0] == 1 && relativeHeights[1] == 1 && relativeHeights[2] == 0 && relativeHeights[3] == 1)
                spriteID = 371;
            else if (relativeHeights[0] == 0 && relativeHeights[1] == 1 && relativeHeights[2] == 1 && relativeHeights[3] == 0)
                spriteID = 372;
            else if (relativeHeights[0] == 1 && relativeHeights[1] == 1 && relativeHeights[2] == 1 && relativeHeights[3] == 0)
                spriteID = 373;
            else if (relativeHeights[0] == 0 && relativeHeights[1] == 1 && relativeHeights[2] == 1 && relativeHeights[3] == 1)
                spriteID = 374;
            else if (relativeHeights[0] == 1 && relativeHeights[1] == 2 && relativeHeights[2] == 1 && relativeHeights[3] == 0)
                spriteID = 375;
            else if (relativeHeights[0] == 2 && relativeHeights[1] == 1 && relativeHeights[2] == 0 && relativeHeights[3] == 1)
                spriteID = 377; //SKIP 376
            else if (relativeHeights[0] == 0 && relativeHeights[1] == 1 && relativeHeights[2] == 2 && relativeHeights[3] == 1)
                spriteID = 378;
            else
                throw new Exception("Unknown tile. (" + relativeHeights[0] + "/" + relativeHeights[1] + "/" + relativeHeights[2] + "/" + relativeHeights[3] + ")");

            Sprite.AtlasRectangle = tileSet.SpriteRectangles[spriteID];

            Sprite.Size = Sprite.AtlasRectangle.Size;

            UpdatePosition();
        }

        public void SetTileSet(TileSet tileSet)
        {
            this.tileSet = tileSet;
            Sprite.Texture = tileSet.TextureAtlas;
            SetHeights(Heights[0], Heights[1], Heights[2], Heights[3]);
        }
    }
}


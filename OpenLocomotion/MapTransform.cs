﻿using System;
using GravityEngine;
using OpenTK;

namespace OpenLocomotion
{
	public class MapTransform : Component
	{
		private Vector3 mapPosition;
		public Vector3 Position
		{
			get
			{
                return Entity.Transform.Position;
			}
			set
			{
                mapPosition = value;
                Entity.Transform.Position = Map.IsoToScreen(value);
            }
		}

		public MapTransform(Entity entity) : base(entity)
        {
		}
	}
}
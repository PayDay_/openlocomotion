﻿using System;
using System.IO;
using System.Media;
using System.Collections.Generic;

namespace OpenLocomotion
{
    public static class MusicPlayer
    {
        static SoundPlayer musicPlayer;
        static Dictionary<string, Song> songs = new Dictionary<string, Song>();

        public static void ImportFromLoco(string directory)
        {
            if (!Directory.Exists(directory))
                throw new FileNotFoundException("Wrong music directory!");

            if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "Content", "Music")))
                Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "Content", "Music"));

            string[] musicFiles = { "20s1",
                                    "20s2",
                                    "20s3",
                                    "20s4",
                                    "20s5",
                                    "20s6",
                                    "40s1",
                                    "40s2",
                                    "40s3",
                                    "50s1",
                                    "50s2",
                                    "50s3",
                                    "60s1",
                                    "60s2",
                                    "60s3",
                                    "70s1",
                                    "70s2",
                                    "70s3",
                                    "80s1",
                                    "80s2",
                                    "80s3",
                                    "80s4",
                                    "90s1",
                                    "90s2",
                                    "css5",
                                    "Chrysanthemum",
                                    "Eugenia",
                                    "rag1",
                                    "rag2",
                                    "rag3",
                                };

            foreach (string file in musicFiles)
            {
                if (File.Exists(Path.Combine("Content", "Music", file + ".wav")))
                    continue;
                string filePath = Path.Combine(directory, file + ".dat");
                File.Copy(filePath, Path.Combine(Environment.CurrentDirectory, Path.Combine("Content", "Music", file + ".wav")));
            }


            List<Song> songs = new List<Song>();
            songs.Add(new Song("Chuggin' Along", "20s1", SongCentury.C20s));
            songs.Add(new Song("Long Dusty Road", "20s2", SongCentury.C20s));
            songs.Add(new Song("Flying High", "20s3", SongCentury.C20s));
            songs.Add(new Song("Gettin' On The Gas", "20s4", SongCentury.C20s));
        }

        public static void Play(string songName)
        {
            if (musicPlayer != null)
                musicPlayer.Stop();

            musicPlayer = new SoundPlayer(Path.Combine(Environment.CurrentDirectory, "Content", "Music", songName + ".wav"));
            musicPlayer.Play();
        }

        public static void Stop()
        {
            if (musicPlayer != null)
                musicPlayer.Stop();
        }
    }

    public enum SongCentury
    {
        C20s,
        C40s,
        C50s,
        C60s,
        C70s,
        C80s,
        C90s,
        Misc
    }

    public class Song
    {
        public Song(string Name, string Path, SongCentury Century)
        {
            this.Name = Name;
            this.Path = Path;
            this.Century = Century;
        }

        public string Name;
        public string Path;
        public SongCentury Century;
    }
}
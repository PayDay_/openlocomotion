﻿using System;
using GravityEngine;
using System.Drawing;
using OpenTK;
using OpenTK.Input;

namespace OpenLocomotion
{
	public class GameCamera : Entity
	{
        private static Vector2 lastMousePos;
        private static Point clickedMousePos;
		public static Vector3 CurrentPos;

		Camera camera;

		public GameCamera ()
		{
			camera = new Camera (this);
			camera.ClearColor = Color.Black;
		}

		protected override void Update ()
		{
			base.Update ();

			UpdatePosition ();
			UpdateZoom ();
		}

		void UpdatePosition()
		{
            if (Input.Mouse.IsButtonPressed(MouseButton.Right))
            {
                clickedMousePos = new Point(Window.Window.Mouse.X, Window.Window.Mouse.Y);
                lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
                Window.Window.CursorVisible = false;
            }

			if (Input.Mouse.IsButtonDown (MouseButton.Right)) 
			{
                Vector2 mousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
                Vector2 delta = mousePos - lastMousePos;
                lastMousePos = mousePos;
                Transform.Position += new Vector3(-delta.X, delta.Y, 0);

                Input.Mouse.Center();
            }

            if (Input.Mouse.IsButtonReleased(MouseButton.Right))
            {
                Input.Mouse.Position = clickedMousePos;
                Window.Window.CursorVisible = true;
            }

			CurrentPos = Map.ScreenToIso (Transform.Position);
			//Console.WriteLine (CurrentPos);
			int tileX = (int)Math.Round (CurrentPos.X);
			int tileY = (int)Math.Round (CurrentPos.Y);
			//Console.WriteLine (tileX + " | " + tileY);
			int chunkX = tileX / 16;
			int chunkY = tileY / 16;
			tileX -= chunkX * 16;
			tileY -= chunkY * 16;

			//Map.Chunks[chunkX, chunkY].Tiles[tileX, tileY].Sprite.Color = new Vector4(1, 0, 0, 1);
		}

		void UpdateZoom()
		{
			if(Input.Keyboard.IsKeyDown(Key.Up))
				camera.OrthographicSize += Time.ElapsedSeconds;
			else if(Input.Keyboard.IsKeyDown(Key.Down))
				camera.OrthographicSize -= Time.ElapsedSeconds;

            camera.OrthographicSize = camera.OrthographicSize;

			//camera.OrthographicSize += Input.Mouse.ScrollDelta * 0.001f;
		}
	}
}


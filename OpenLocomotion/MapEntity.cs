﻿using GravityEngine;
using OpenTK;

namespace OpenLocomotion
{
	public class MapEntity : Entity
	{
		new public MapTransform Transform;

		public MapEntity(Transform parent) : base(Space.World, parent)
		{
			//Transform.Destroy();
			Transform = new MapTransform(this);
		}
	}
}
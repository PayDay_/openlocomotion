﻿using System;
using GravityEngine;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace OpenLocomotion
{
	public class Window : GravityWindow
	{
		static GameCamera camera;
        static Label labelDrawCalls;
        static Label labelFPS;
		static GUIWindow window;

        public static GravityEngine.Font SourceSansPro;
        public static GravityEngine.Font SourceSansProBold;

        public Window () : base ()
		{
			
		}

		protected override void OnLoad (EventArgs e)
		{
			base.OnLoad (e);

            GameCursor.Init();
            SourceSansPro = new GravityEngine.Font("Fonts/SourceSansPro-Regular.ttf", 40);
            SourceSansProBold = new GravityEngine.Font("Fonts/SourceSansPro-Bold.ttf", 40);

            camera = new GameCamera ();
            Map.Generate();
			Track.Load();
			Debug.Init();
			Debug.Execution += ExecuteCommand;

            Entity labelEntity = new Entity(new Vector3(5, 5, -500), null);
            labelEntity.Transform.Space = Space.Screen;
            labelDrawCalls = new Label(labelEntity, "DCs: ", SourceSansPro, 20, StringAlignment.Near, Vector4.One);

            labelEntity = new Entity(new Vector3(795, 5, -500), null);
            labelEntity.Transform.Space = Space.Screen;
            labelFPS = new Label(labelEntity, "FPS: ", SourceSansPro, 20, StringAlignment.Far, Vector4.One);
            //MusicPlayer.ImportFromLoco("D:/Spiele/Locomotion/Data/");
            //MusicPlayer.Play("90s2");

            labelEntity = new Entity(new Vector3(700, 700, -500), null);
            labelEntity.Transform.Space = Space.Screen;
            new Label(labelEntity, "Hello World!", SourceSansPro, 40, StringAlignment.Far, Vector4.One);

            window = new GUIWindow("Test", new Size(400, 400));
            window.Transform.Position = new Vector3(200, 200, window.Transform.Position.Z);
        }

		protected override void OnRenderFrame (OpenTK.FrameEventArgs e)
		{
			base.OnRenderFrame (e);

            if (labelDrawCalls != null)
                labelDrawCalls.Text = "DCs: " + DrawCalls;

            if (labelFPS != null)
                labelFPS.Text = Math.Round(Time.FramesPerSecond, 2) + " FPS";
        }

		internal void ExecuteCommand(ExecutionEventArgs e)
		{
			if (e.Command == "createTracks")
			{
				#region Test-Track
				Track testTrack = new Track(Track.TrackType.STRAIGHT1);
				testTrack.Transform.Position = new Vector3(0, 0, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT1);
				testTrack.Transform.Position = new Vector3(0, 1, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT1);
				testTrack.Transform.Position = new Vector3(0, 2, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT1);
				testTrack.Transform.Position = new Vector3(0, 3, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT1);
				testTrack.Transform.Position = new Vector3(0, 4, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT1);
				testTrack.Transform.Position = new Vector3(0, 5, 0);
				testTrack = new Track(Track.TrackType.CURVE_SMALL1);
				testTrack.Transform.Position = new Vector3(0, 6, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT2);
				testTrack.Transform.Position = new Vector3(2, 7, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT2);
				testTrack.Transform.Position = new Vector3(3, 7, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT2);
				testTrack.Transform.Position = new Vector3(4, 7, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT2);
				testTrack.Transform.Position = new Vector3(5, 7, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT2);
				testTrack.Transform.Position = new Vector3(6, 7, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT2);
				testTrack.Transform.Position = new Vector3(7, 7, 0);
				testTrack = new Track(Track.TrackType.STRAIGHT2);
				testTrack.Transform.Position = new Vector3(8, 7, 0);
				#endregion
			}
			if (e.Command.StartsWith("import ", StringComparison.CurrentCulture))
			{
				string[] args = e.Command.Split(' ');
				ObjImport.Import(ObjImport.Load(args[1]));
				Debug.Log("Imported!");
			}
			if (e.Command == "bullshit")
			{
				Debug.Log("Some bullshit got executed by user!");
			}
		}
	}
}

